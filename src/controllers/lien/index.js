import abind from 'abind'
import { StatusCodes } from 'http-status-codes'

import { err, ok } from '../../utils/error'

export class LienController {
  constructor (lienService) {
    this.lienService = lienService
    abind(this)
  }

  async create (req, res) {
    const { msg } = req.body
    const {
      plot_int: plotInt,
      lien_holder: lienHolder,
      lien_amount: lienAmount,
      sender_address: lienSender,
    } = JSON.parse(msg)

    try {
      const response = await this.lienService.create({ plotInt, lienHolder, lienAmount, lienSender })
      res.status(StatusCodes.OK).json(ok(response))
    } catch (error) {
      res.status(error.code ?? StatusCodes.INTERNAL_SERVER_ERROR).json(err(error))
    }
  }
}
