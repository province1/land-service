import abind from 'abind'
import { StatusCodes } from 'http-status-codes'

import { err, ok } from '../../utils/error'

export class LandController {
  constructor (landService) {
    this.landService = landService
    abind(this)
  }

  async clearLand (req, res) {
    const { msg } = req.body
    const { plot_int: plotInt, sender_address: senderAddress } = JSON.parse(msg)
    try {
      const response = await this.landService.startClearing({ plotInt, senderAddress })
      res.status(StatusCodes.OK).json(ok(response))
    } catch (error) {
      res.status(error.code ?? StatusCodes.INTERNAL_SERVER_ERROR).json(err(error))
    }
  }

  async startLandSale (req, res) {
    const { msg } = req.body
    const { plot_int: plotInt } = JSON.parse(msg)

    try {
      const response = await this.landService.placeEscrowLock({ plotInt })
      res.status(StatusCodes.OK).json(ok(response))
    } catch (error) {
      res.status(error.code ?? StatusCodes.INTERNAL_SERVER_ERROR).json(err(error))
    }
  }

  async finalizeLandSale (req, res) {
    const { force_as: forceAs } = req.query
    const {
      plot_int: plotInt,
      sale_id: saleId,
    } = req.params
    try {
      const response = await this.landService.removeEscrowLock({ plotInt, saleId, forceAs })
      res.status(StatusCodes.OK).json(ok(response))
    } catch (error) {
      res.status(error.code ?? StatusCodes.INTERNAL_SERVER_ERROR).json(err(error))
    }
  }

  async stopClearLand (req, res) {
    const { plot_int: plotInt, owner } = req.params
    try {
      const response = await this.landService.stopClearing({ plotInt, owner })
      res.status(StatusCodes.OK).json(ok(response))
    } catch (error) {
      res.status(error.code ?? StatusCodes.INTERNAL_SERVER_ERROR).json(err(error))
    }
  }

  async execStatusChecks (req, res) {
    const { plot_int: plotInt } = req.params
    try {
      const response = await this.landService.doStatusChecksWithUpdates({ plotInt })
      res.status(StatusCodes.OK).json(ok(response))
    } catch (error) {
      res.status(error.code ?? StatusCodes.INTERNAL_SERVER_ERROR).json(err(error))
    }
  }
}
