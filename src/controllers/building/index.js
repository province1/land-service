import abind from 'abind'
import { StatusCodes } from 'http-status-codes'

import { err, ok } from '../../utils/error'

export class BuildingController {
  constructor (buildingService) {
    this.buildingService = buildingService
    abind(this)
  }

  async create (req, res) {
    const { msg } = req.body
    const { building_type: buildingType } = req.params
    const {
      plot_int: plotInt,
      sender_address: senderAddress,
    } = JSON.parse(msg)

    try {
      const response = await this.buildingService.create({ plotInt, senderAddress, buildingType })
      res.status(StatusCodes.OK).json(ok(response))
    } catch (error) {
      res.status(error.code ?? StatusCodes.INTERNAL_SERVER_ERROR).json(err(error))
    }
  }
}
