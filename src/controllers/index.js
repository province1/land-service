import { HealthCheckService } from '../services/health-check'
import { ownershipService, lienService, landService, buildingService } from '../services'

import { PublicController } from './public'
import { OwnershipController } from './ownership'
import { LienController } from './lien'
import { LandController } from './land'
import { BuildingController } from './building'

export const publicController = new PublicController(new HealthCheckService())
export const ownershipController = new OwnershipController(ownershipService)
export const lienController = new LienController(lienService)
export const landController = new LandController(landService)
export const buildingController = new BuildingController(buildingService)
