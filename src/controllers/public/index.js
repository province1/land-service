import { StatusCodes } from 'http-status-codes'
import abind from 'abind'

export class PublicController {
  constructor (healthCheckService) {
    this.healthCheckService = healthCheckService
    abind(this)
  }

  healthCheck (_, res) {
    const uptime = this.healthCheckService.getUptime()
    res.status(StatusCodes.OK).json(uptime)
  }
}
