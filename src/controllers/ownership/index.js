import abind from 'abind'
import { StatusCodes } from 'http-status-codes'

import { err, ok } from '../../utils/error'

export class OwnershipController {
  constructor (ownershipService) {
    this.ownershipService = ownershipService
    abind(this)
  }

  async changeOwnership (req, res) {
    const { plot_int: plotInt, new_owner: newOwner } = req.body
    const servicePayload = { plotInt, newOwner }
    try {
      const response = await this.ownershipService.change(servicePayload)
      res.status(StatusCodes.OK).json(ok(response))
    } catch (error) {
      res.status(error.code ?? StatusCodes.INTERNAL_SERVER_ERROR).json(err(error))
    }
  }
}
