import runLoaders from './loaders'

export async function appBuilder () {
  return await runLoaders()
}
