export default {
  plot_int: {
    in: ['body'],
    isNumeric: {
      options: {
        no_symbols: true,
      },
    },
    toInt: true,
  },
  new_owner: {
    in: ['body'],
    isHexadecimal: true,
    isLength: {
      options: {
        max: 50,
        min: 40,
      },
    },
  },
}
