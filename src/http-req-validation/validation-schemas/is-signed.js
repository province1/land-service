import { isNil } from 'lodash'
import { RequestValidationError } from '../../exceptions/request-validation'

import { isHex } from '../../utils/type-checks/is-hex'
import { isNumericOrNumericString } from '../../utils/type-checks/is-numeric-or-numeric-string'

export default {
  msg: {
    in: ['body'],
    notEmpty: true,
    custom: {
      options: (value) => {
        let payload = {}
        try {
          payload = JSON.parse(value)
        } catch {
          throw new RequestValidationError('Message is not a valid serialized json object')
        }

        if (isNil(payload?.sender_address)) {
          throw new RequestValidationError('Expected sender_address attribute is not present within message')
        }

        if (!isHex(payload.sender_address)) {
          throw new RequestValidationError('Expected sender_address is invalid')
        }

        if (
          isNil(payload?.timestamp) ||
          !isNumericOrNumericString(payload?.timestamp)
        ) {
          throw new RequestValidationError('Expected timestamp is not present or invalid')
        }
        return true
      },
    },
  },
  signature: {
    in: ['body'],
    notEmpty: true,
  },
}
