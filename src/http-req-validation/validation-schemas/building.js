import { RequestValidationError } from '../../exceptions/request-validation'
import { isNumericOrNumericString } from '../../utils/type-checks/is-numeric-or-numeric-string'

export const addBuildingSchema = {
  building_type: {
    in: ['params'],
    optional: false,
    notEmpty: true,
    isIn: {
      options: [['home', 'lumber-mill', 'warehouse', 'grain-storage', 'farm']],
    },
  },
  msg: {
    in: ['body'],
    notEmpty: true,
    custom: {
      options: (value) => {
        let payload = {}
        try {
          payload = JSON.parse(value)
        } catch {
          throw new RequestValidationError('Message is not a valid serialized json object')
        }
        if (!isNumericOrNumericString(payload?.plot_int)) {
          throw new RequestValidationError('Expected plot_int attribute is not valid')
        }
        return true
      },
    },
  },
}
