import { isNil } from 'lodash'

import { RequestValidationError } from '../../exceptions/request-validation'
import { isHex } from '../../utils/type-checks/is-hex'
import { isNumericOrNumericString } from '../../utils/type-checks/is-numeric-or-numeric-string'

export default {
  msg: {
    in: ['body'],
    notEmpty: true,
    custom: {
      options: (value) => {
        let payload = {}
        try {
          payload = JSON.parse(value)
        } catch {
          throw new RequestValidationError('Message is not a valid serialized json object')
        }
        if (!isNumericOrNumericString(payload?.plot_int)) {
          throw new RequestValidationError('Expected plot_int attribute is not valid')
        }
        if (isNil(payload?.lien_holder) || !isHex(payload?.lien_holder)) {
          throw new RequestValidationError('Expected lien_holder is not valid')
        }
        if (!isNumericOrNumericString(payload?.lien_amount)) {
          throw new RequestValidationError('Expected plot_int attribute is not valid')
        }

        return true
      },
    },
  },
}
