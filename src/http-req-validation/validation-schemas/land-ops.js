import { RequestValidationError } from '../../exceptions/request-validation'
import { isNumericOrNumericString } from '../../utils/type-checks/is-numeric-or-numeric-string'

export const clearLandSchema = {
  msg: {
    in: ['body'],
    notEmpty: true,
    custom: {
      options: (value) => {
        let payload = {}
        try {
          payload = JSON.parse(value)
        } catch {
          throw new RequestValidationError('Message is not a valid serialized json object')
        }
        if (!isNumericOrNumericString(payload?.plot_int)) {
          throw new RequestValidationError('Expected plot_int attribute is not valid')
        }

        return true
      },
    },
  },
}

export const stopClearLandSchema = {
  plot_int: {
    in: ['params'],
    isNumeric: {
      options: {
        no_symbols: true,
      },
    },
    toInt: true,
  },
  owner: {
    in: ['params'],
    isHexadecimal: true,
    isLength: {
      options: {
        max: 50,
        min: 40,
      },
    },
  },
}

export const doLandChecksSchema = {
  plot_int: {
    in: ['params'],
    isNumeric: {
      options: {
        no_symbols: true,
      },
    },
    toInt: true,
  },
}

export const startLandSaleSchema = {
  msg: {
    in: ['body'],
    notEmpty: true,
    custom: {
      options: (value) => {
        let payload = {}
        try {
          payload = JSON.parse(value)
        } catch {
          throw new RequestValidationError('Message is not a valid serialized json object')
        }
        if (!isNumericOrNumericString(payload?.plot_int)) {
          throw new RequestValidationError('Expected plot_int attribute is not valid')
        }

        return true
      },
    },
  },
}

export const finalizeLandSaleSchema = {
  force_as: {
    in: ['query'],
    isIn: {
      options: [['cancelled', 'aborted']],
    },
    optional: true,
  },
  sale_id: {
    in: ['params'],
    isUUID: {
      options: 4,
    },
  },
  plot_int: {
    in: ['params'],
    isNumeric: {
      options: {
        no_symbols: true,
      },
    },
    toInt: true,
  },
}
