import { database_service as databaseService } from 'data-service-adapter'

import { LandDatabaseService } from './land-events-database'
import { SalesDatabaseService } from './sales-events-database'
import { BuildingDatabaseService } from './building-event-database'
import {
  EVENTS_DATABASE_SERVICE_PASSWORD,
  EVENTS_DATABASE_SERVICE_URL,
  EVENTS_DATABASE_SERVICE_USERNAME,
} from '../config'

const dbConnector = databaseService(
  EVENTS_DATABASE_SERVICE_URL,
  EVENTS_DATABASE_SERVICE_USERNAME,
  EVENTS_DATABASE_SERVICE_PASSWORD,
)

export const landDatabaseService = new LandDatabaseService(dbConnector)
export const saleDatabaseService = new SalesDatabaseService(dbConnector)
export const buildingDatabaseService = new BuildingDatabaseService(dbConnector)
