import abind from 'abind'
import { isEmpty, pick } from 'lodash'
import camelize from 'camelize'
import decamilize from 'decamelize-keys'

import { EventsDatabaseError } from '../exceptions/events-database'

// This is the wrapper class around db_service_adapter
// goal: to hide all the details of using the adapter directly and provide
// an internal contract of events database within the application

export class EventsDatabaseService {
  constructor (dbConnector) {
    this.dbConnector = dbConnector
    abind(this)
    this.fieldsList = []
  }

  _convertToAdapterState (appState) {
    return pick(decamilize(appState), this.fieldsList)
  }

  _convertToAppState (stateFromAdapter) {
    const stateForApp = camelize(pick(stateFromAdapter, this.fieldsList))

    return isEmpty(stateForApp) ? undefined : stateForApp
  }

  async sendOperation (payload, operation) {
    try {
      const response = await this.dbConnector.data_op(payload, operation)

      if (response?.error === true) { throw new Error(response?.message ?? response?.error_msg) }

      return response?.result
    } catch (error) {
      throw new EventsDatabaseError('Sending of an operation failed', error)
    }
  }
}
