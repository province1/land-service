import abind from 'abind'
import { isEmpty, isNil } from 'lodash'

import { EventsDatabaseError } from '../exceptions/events-database'
import { EventsDatabaseService } from './events-database'

export class BuildingDatabaseService extends EventsDatabaseService {
  constructor (...args) {
    super(...args)
    abind(this)
    this.fieldsList = [
      'description',
      'model',
      'object',
      'operational',
      'owner',
      'timestamp',
      'serial',
      'event',
    ]
  }

  async generateNewBuilding (stateForNewBuilding) {
    try {
      const effectiveStateForNewBuilding = { ...stateForNewBuilding, event: 'create_building' }
      if (isNil(stateForNewBuilding.serial)) {
        const currentState = await this.getCurrentState()
        effectiveStateForNewBuilding.serial = Number(currentState.serial) + 1
      }
      await this.putNewState(effectiveStateForNewBuilding)
    } catch (error) {
      throw new EventsDatabaseError('Unable to create new building', error)
    }
  }

  async getCurrentState (serial = undefined) {
    const extraQuerySearch = isNil(serial) ? { } : { query: { serial: Number(serial) } }
    const payload = {
      collection: 'buildings',
      ...extraQuerySearch,
    }
    const operation = 'current_state'

    try {
      const response = await this.sendOperation(payload, operation)

      return response instanceof Array ? this._convertToAppState(response[0]) : undefined
    } catch (error) {
      throw new EventsDatabaseError('Unable to get current state for a building', error)
    }
  }

  async putNewState (appState = {}) {
    const stateForAdapter = this._convertToAdapterState(appState)
    if (isEmpty(stateForAdapter)) { return }

    const payload = {
      collection: 'buildings',
      entry_doc: stateForAdapter,
    }
    const operation = 'state_change'

    try {
      const response = await this.sendOperation(payload, operation)

      return response?.result ?? response
    } catch (error) {
      throw new EventsDatabaseError('Unable to update building state', error)
    }
  }
}
