import abind from 'abind'
import { isEmpty, isNil } from 'lodash'

import { EventsDatabaseError } from '../exceptions/events-database'
import { EventsDatabaseService } from './events-database'

export class LandDatabaseService extends EventsDatabaseService {
  constructor (...args) {
    super(...args)
    abind(this)

    this.buildingTypeMap = {
      1: 'home',
      2: 'lumber-mill',
      3: 'warehouse',
      4: 'grain-storage',
      5: 'farm',
      home: 1,
      'lumber-mill': 2,
      warehouse: 3,
      'grain-storage': 4,
      farm: 5,
    }

    this.fieldsList = [
      'building_type',
      'fertility',
      'id',
      'escrow',
      'land_status',
      'lien',
      'lien_holder',
      'lien_amount',
      'minerals',
      'owner',
      'plot_int',
      'timestamp',
      'clearing_at',
      'cleared_at',
    ]
  }

  _convertToAppState (stateFromAdapter) {
    const appState = super._convertToAppState(stateFromAdapter)
    if (!isNil(appState.buildingType)) {
      appState.buildingType = this.buildingTypeMap[stateFromAdapter.buildingType]
    }

    return appState
  }

  _convertToAdapterState (stateFromApp) {
    const adapterState = super._convertToAdapterState(stateFromApp)
    if (!isNil(adapterState.building_type)) {
      adapterState.building_type = this.buildingTypeMap[stateFromApp.buildingType]
    }

    return adapterState
  }

  async getCurrentState (plotInt = undefined) {
    const extraQuerySearch = isNil(plotInt) ? { } : { query: { plot_int: Number(plotInt) } }
    const payload = {
      collection: 'land_data',
      ...extraQuerySearch,
    }
    const operation = 'current_state'

    try {
      const response = await this.sendOperation(payload, operation)

      return response instanceof Array ? this._convertToAppState(response[0]) : undefined
    } catch (error) {
      throw new EventsDatabaseError('Unable to get current state for a land', error)
    }
  }

  async putNewState (appState = {}) {
    const stateForAdapter = this._convertToAdapterState(appState)
    if (isEmpty(stateForAdapter)) { return }

    const payload = {
      collection: 'land_data',
      entry_doc: stateForAdapter,
    }
    const operation = 'state_change'

    try {
      const response = await this.sendOperation(payload, operation)

      return response?.result ?? response
    } catch (error) {
      throw new EventsDatabaseError('Unable to update land state', error)
    }
  }
}
