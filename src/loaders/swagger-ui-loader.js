import swaggerUi from 'swagger-ui-express'

import { swaggerSpecification } from '../utils/open-api-spec-builder'

export default function ({ app }) {
  app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpecification))

  return app
}
