import { json as jsonMiddleware, urlencoded } from 'express'
import cors from 'cors'
import morgan from 'morgan'

import { ALLOWED_CORS_ORIGINS_REGEX, PORT } from '../config'

export default ({ app }) => {
  app.set('port', PORT)
  app.use(
    cors({
      origin: ALLOWED_CORS_ORIGINS_REGEX,
      credentials: true,
    }),
  )
  app.use(jsonMiddleware())
  app.use(urlencoded({ extended: false }))
  app.use(morgan('combined'))

  return app
}
