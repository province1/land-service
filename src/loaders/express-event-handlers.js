import ErrorHandler, { NotFoundHandler } from '../exceptions/handler'

export default function ({ app }) {
  app.use(ErrorHandler)
  app.use(NotFoundHandler)

  return app
};
