import express from 'express'
import expressConfigLoader from './express'
import routesMounter from './api-routes'
import eventHandlersLoader from './express-event-handlers'
import swaggerUiLoader from './swagger-ui-loader'

export default async function () {
  const app = express()
  expressConfigLoader({ app })
  swaggerUiLoader({ app })
  routesMounter({ app })
  eventHandlersLoader({ app })

  return { app }
}
