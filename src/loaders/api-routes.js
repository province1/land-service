import apiRoutesMounter from '../routes'

export default ({ app }) => {
  return apiRoutesMounter({ app })
}
