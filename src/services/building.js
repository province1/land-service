import abind from 'abind'
import { isEmpty, isNil } from 'lodash'

import { EventsDatabaseUnexpectedStateError } from '../exceptions/events-database'
import { OperationalError } from '../exceptions/operational'
import { BuildingRules } from './building-rules'
import { accountingService } from '.'
import { nowInMilliseconds, realTimeToProvinceTime } from '../utils/date'
import { LandRules } from './land-rules'

export class BuildingService {
  constructor (externalLandDatabaseService, externalBuildingDatabaseService, materialsApiClient) {
    this.landDatabaseService = externalLandDatabaseService
    this.buildingDatabaseService = externalBuildingDatabaseService
    this.materialsApiClient = materialsApiClient
    abind(this)
  }

  async _getOwnerBalanceForResources ({ owner, materialsForBuilding }) {
    const ownedResourcesPromises = materialsForBuilding.map(
      async ([resource, requiredAmount]) => (
        [
          resource,
          requiredAmount,
          await this.materialsApiClient.balance({ owner, materialType: resource }),
        ]
      ),
    )
    return Promise.all(ownedResourcesPromises)
  }

  async checkEnoughResources ({ owner, materialsForBuilding }) {
    const ownedResources = await this._getOwnerBalanceForResources({ owner, materialsForBuilding })
    const missingResources = ownedResources.filter(
      ([, requiredAmount, ownedAmount]) => (requiredAmount > ownedAmount),
    )
    if (missingResources.length > 0) {
      const missingStatus = missingResources
        .map(([resource, required, owned]) => `${resource} missing ${required - owned}`)
        .join(';')
      throw new OperationalError(`Not enough resources: ${missingStatus}`)
    }
  }

  async createMaterials ({ owner, materials }) {
    try {
      await Promise.all(
        materials.map(([materialType, amount]) => this.materialsApiClient.create({
          owner,
          type: materialType,
          amount,
        })),
      )
    } catch (error) {
      throw new OperationalError('Unable to create materials', error)
    }
  }

  async destroyMaterialsRequiredForBuildingWithRollbackOnFailure ({ owner, materialsForBuilding }) {
    const destroyedMaterials = []
    let wasAbleToDestroyAllTheMaterials = true
    let lastError

    for (const material of materialsForBuilding) {
      if (!wasAbleToDestroyAllTheMaterials) { continue }
      const [materialType, amount] = material
      try {
        await this.materialsApiClient.destroy({ owner, type: materialType, amount })
        destroyedMaterials.push(material)
      } catch (error) {
        lastError = error
        wasAbleToDestroyAllTheMaterials = false
      }
    }

    if (!wasAbleToDestroyAllTheMaterials) {
      // rollback on failure create destroyed materials
      try {
        await this.createMaterials({ owner, materials: destroyedMaterials })
      } catch (error) {
        throw new OperationalError(
          'Severe error! possible inconsistency. Unable to return back materials after failed destroy',
          error,
        )
      }
      throw new OperationalError('Unable to destroy all required materials for the building', lastError)
    }
  }

  async create ({ plotInt, senderAddress, buildingType }) {
    const landCurrentState = await this.landDatabaseService.getCurrentState(plotInt)

    if (isEmpty(landCurrentState)) {
      throw new EventsDatabaseUnexpectedStateError('Empty sate found for plot')
    }

    if (`${senderAddress}`.toLowerCase() !== `${landCurrentState?.owner}`.toLowerCase()) {
      throw new OperationalError('Only owner is allowed to create a building')
    }

    if (landCurrentState.landStatus !== 'cleared') {
      throw new OperationalError('Cannot start a building, land is not cleared')
    }

    if (!isNil(landCurrentState.clearedAt) && LandRules.rule('tree', 'grow-limit-reached', landCurrentState.clearedAt)) {
      throw new OperationalError('Too hight trees on the land, unable to start building')
    }

    const costOfABuildingInMaple = BuildingRules.rule(buildingType, 'maple-cost')
    const requiredBuildingMaterials = BuildingRules.rule(buildingType, 'materials')

    await this.checkEnoughResources({ owner: senderAddress, materialsForBuilding: requiredBuildingMaterials })

    const senderMapleBalance = await accountingService.addressBalance(senderAddress)

    if (senderMapleBalance < costOfABuildingInMaple) {
      throw new OperationalError('Not enough maple for building')
    }

    try {
      await this.destroyMaterialsRequiredForBuildingWithRollbackOnFailure({
        owner: senderAddress,
        materialsForBuilding: requiredBuildingMaterials,
      })
      await this.landDatabaseService.putNewState({
        ...landCurrentState,
        landStatus: 'building',
        buildingType,
      })
      await accountingService.transferOfAmountFromAddressToTreasury({
        sender: senderAddress,
        amount: costOfABuildingInMaple,
      })

      // TODO: how to get model, description, operational attributes?
      await this.buildingDatabaseService.generateNewBuilding({
        description: `TBD: building for the ${senderAddress}`,
        model: `TBD some model for ${senderAddress}`,
        owner: senderAddress,
        object: buildingType,
        timestamp: realTimeToProvinceTime(nowInMilliseconds()),
      })
    } catch (error) {
      // rollback land state
      await this.landDatabaseService.putNewState(landCurrentState)
      // TODO: might happen after doing a successful transfer to treasury
      // generateNewBuilding can fail. So what to do for recovering? this "transaction"
      // must to be completed , since all the steps were ok.
      // Or revert back all the other transactions
      //   revert: transferOfAmountFromAddressToTreasury
      //   revert: destroyMaterialsRequiredForBuildingWithRollbackOnFailure
      // Seems to be simpler to just create a failed generateNewBuilding may be by
      // manual intervention at least for now
      throw error
    }
  }
}
