import abind from 'abind'
import Moralis from 'moralis/node'

import { TREASURY_ADDRESS } from '../config'
import { EthersContractOperationsError } from '../exceptions/ethers'
import { OperationalError } from '../exceptions/operational'

const ethers = Moralis.web3Library

export class AccountingService {
  constructor (mapleContactFactory, fundsApiClient) {
    this.mapleContactFactory = mapleContactFactory
    this.fundsApiClient = fundsApiClient
    abind(this)
  }

  async getAllowanceForTreasury (address) {
    try {
      const ethersMapleContract = this.mapleContactFactory.produceSingleton()
      const rawAllowance = await ethersMapleContract.allowance(address, TREASURY_ADDRESS)

      return parseInt(ethers.utils.formatEther(rawAllowance))
    } catch (error) {
      throw new EthersContractOperationsError(error)
    }
  }

  async addressBalance (address) {
    try {
      const ethersMapleContract = this.mapleContactFactory.produceSingleton()
      const rawBalance = await ethersMapleContract.balanceOf(address)

      return parseInt(ethers.utils.formatEther(rawBalance))
    } catch (error) {
      throw new EthersContractOperationsError(error)
    }
  }

  async transferOfAmountFromAddressToTreasury ({ sender, amount }) {
    const allowedAmount = await this.getAllowanceForTreasury(sender)
    if (allowedAmount < amount) {
      throw new OperationalError('allowed amount is less than required, aborting the transfer')
    }

    await this.fundsApiClient.transferAmount({ sender, amount })
  }
}
