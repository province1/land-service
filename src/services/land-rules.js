import { isNil } from 'lodash'

import { DAYS_IN_YEAR_FLOAT, MILLISECONDS_IN_A_DAY } from '../config'
import { nowInMilliseconds, realTimeToProvinceTime } from '../utils/date'

// game rules for land

// real speed for fast growing tress is 200 centimeters per year
// 2000 millimeters / 365.25 days
// average tree heigh is 20 meters = 20000 millimeters
const TREE_GROWTH_SPEED_PER_DAY = 2000 / DAYS_IN_YEAR_FLOAT
const TREE_GROWTH_SPEED_PER_MILLISECOND = TREE_GROWTH_SPEED_PER_DAY / MILLISECONDS_IN_A_DAY
const TREE_AVERAGE_HEIGH_IN_MILLIMETERS = 20000

export class LandRules {
  static rule (type, ruleName, ...args) {
    const ruleId = [type, ruleName].filter(r => !isNil(r)).join('.')
    const rules = {
      'tree.grow-limit-reached': (clearedAtInProvinceTime) => {
        const now = realTimeToProvinceTime(nowInMilliseconds())
        const growingIntervalInMilliseconds = now - clearedAtInProvinceTime
        const currentTreeHeigh = TREE_GROWTH_SPEED_PER_MILLISECOND * growingIntervalInMilliseconds
        const heighLimit = LandRules.rule('tree', 'limit-for-forest')

        return currentTreeHeigh >= heighLimit
      },
      'tree.limit-for-forest': () => {
        return TREE_AVERAGE_HEIGH_IN_MILLIMETERS * 0.1 // 10%
      },
    }

    if (isNil(rules[ruleId]) || !(rules[ruleId] instanceof Function)) {
      throw new Error(`invalid or undefined rule ${ruleId}`)
    }

    return rules[ruleId](...args)
  }
}
