export class HealthCheckService {
  getUptime = () => ({
    uptime: new Date().toUTCString(),
  })
}
