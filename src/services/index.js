import { landDatabaseService, saleDatabaseService, buildingDatabaseService } from '../external-db-integrations'
import { materialsApiClient, fundsApiClient } from '../external-api-http-clients'
import { ethersMapleContactFactory } from '../factories'

import { AccountingService } from './accounting'
import { BuildingService } from './building'
import { LandService } from './land'
import { LienService } from './lien'
import { OwnershipService } from './ownership'

export const landService = new LandService(landDatabaseService, saleDatabaseService)
export const lienService = new LienService(landDatabaseService)
export const ownershipService = new OwnershipService(landDatabaseService)
export const accountingService = new AccountingService(ethersMapleContactFactory, fundsApiClient)
export const buildingService = new BuildingService(landDatabaseService, buildingDatabaseService, materialsApiClient)
