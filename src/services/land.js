import abind from 'abind'
import { isEmpty, isNil } from 'lodash'

import { EventsDatabaseUnexpectedStateError } from '../exceptions/events-database'
import { OperationalError } from '../exceptions/operational'
import { realTimeToProvinceTime, nowInMilliseconds } from '../utils/date'
import { LandRules } from './land-rules'

export class LandService {
  constructor (externalLandDatabaseService, externalSaleDatabaseService) {
    this.landDatabaseService = externalLandDatabaseService
    this.saleDatabaseService = externalSaleDatabaseService
    abind(this)
  }

  async startClearing ({ plotInt, senderAddress }) {
    const currentState = await this.landDatabaseService.getCurrentState(plotInt)
    const nowInProvinceTime = realTimeToProvinceTime(nowInMilliseconds())
    if (isEmpty(currentState)) {
      throw new EventsDatabaseUnexpectedStateError('Empty sate found for plot')
    }

    if (`${senderAddress}`.toLowerCase() !== `${currentState?.owner}`.toLowerCase()) {
      throw new OperationalError('Only owner is allowed clear its land')
    }

    if (currentState?.landStatus !== 'forest') {
      if (currentState.landStatus === 'clearing' && isNil(currentState.clearedAt)) {
        throw new OperationalError('The plot is being cleared already')
      } else if (!isNil(currentState.clearedAt) && !LandRules.rule('tree', 'grow-limit-reached', currentState.clearedAt)) {
        throw new OperationalError('Trees height is not enough to start clearing process')
      }
    }

    const result = await this.landDatabaseService.putNewState({
      ...currentState,
      landStatus: 'clearing',
      clearedAt: undefined,
      clearingAt: nowInProvinceTime,
      timestamp: nowInMilliseconds(),
    })

    return result
  }

  async placeEscrowLock ({ plotInt }) {
    const currentState = await this.landDatabaseService.getCurrentState(plotInt)

    if (isEmpty(currentState)) {
      throw new EventsDatabaseUnexpectedStateError('Empty sate found for plot')
    }


    console.log(999, currentState)

    if (currentState?.escrow === true) {
      throw new OperationalError('Land already palaced for a sale')
    }

    if (currentState?.lien === true) {
      throw new OperationalError('Land already has a lien')
    }

    const result = await this.landDatabaseService.putNewState({
      ...currentState,
      escrow: true,
      timestamp: nowInMilliseconds(),
    })

    return result
  }

  async removeEscrowLock ({ plotInt, saleId, forceAs }) {
    const shouldCancel = forceAs === 'cancelled'
    const shouldAbort = forceAs === 'aborted'
    const isNormalEnding = isNil(forceAs)

    const currentState = await this.landDatabaseService.getCurrentState(plotInt)
    const currentSaleState = await this.saleDatabaseService.getCurrentState({ saleId })

    if (isEmpty(currentState)) {
      throw new EventsDatabaseUnexpectedStateError('Empty sate found for plot')
    }

    if (isEmpty(currentSaleState)) {
      throw new EventsDatabaseUnexpectedStateError('Empty sate found for sale')
    }

    const landSeller = currentSaleState?.seller
    const landBuyer = currentSaleState?.buyer

    if (`${landSeller}`.toLowerCase() !== `${currentState?.owner}`.toLowerCase()) {
      throw new OperationalError('Only owner is allowed to finalize a sale')
    }

    if (isNil(currentSaleState?.state)) {
      throw new EventsDatabaseUnexpectedStateError('Sale state is undefined aborting')
    }

    if (shouldCancel && currentSaleState?.state !== 'cancelled') {
      throw new OperationalError('Sale cannot be terminated, cancelled state is expected')
    }

    if ((shouldAbort || isNormalEnding) && currentSaleState?.state !== 'ended') {
      throw new OperationalError('Sale cannot be terminated, ended state is expected')
    }

    if (plotInt !== currentSaleState?.plotInt) {
      throw new OperationalError('Sale plot does not match the requested')
    }

    const newOwner = (shouldCancel || shouldAbort) ? currentState.owner : landBuyer

    const result = await this.landDatabaseService.putNewState({
      ...currentState,
      escrow: false,
      owner: newOwner,
      timestamp: nowInMilliseconds(),
    })

    return result
  }

  async stopClearing ({ plotInt, owner }) {
    const currentState = await this.landDatabaseService.getCurrentState(plotInt)

    if (isEmpty(currentState)) {
      throw new EventsDatabaseUnexpectedStateError('Empty sate found for plot')
    }

    if (`${owner}`.toLowerCase() !== `${currentState?.owner}`.toLowerCase()) {
      throw new OperationalError('Only owner is allowed to stop the clearing')
    }

    const newState = {
      ...currentState,
      landStatus: 'cleared',
      clearedAt: realTimeToProvinceTime(nowInMilliseconds()),
      timestamp: nowInMilliseconds(),
    }
    await this.landDatabaseService.putNewState(newState)

    return newState
  }

  async doStatusChecksWithUpdates ({ plotInt }) {
    const currentState = await this.landDatabaseService.getCurrentState(plotInt)
    let newState = { ...currentState }
    let stateUpdated = false

    if (isEmpty(currentState)) {
      throw new EventsDatabaseUnexpectedStateError('Empty sate found for plot')
    }

    if (currentState.landStatus === 'cleared' && !isNil(currentState.clearedAt)) {
      if (LandRules.rule('tree', 'grow-limit-reached', currentState.clearedAt)) {
        stateUpdated = true
        newState = {
          ...newState,
          landStatus: 'forest',
        }
      }
    }

    if (stateUpdated) {
      await this.landDatabaseService.putNewState(newState)
    }

    return newState
  }
}
