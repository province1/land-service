import abind from 'abind'
import { isEmpty } from 'lodash'

import { EventsDatabaseUnexpectedStateError } from '../exceptions/events-database'
import { nowInMilliseconds } from '../utils/date'
export class LienService {
  constructor (externalLandDatabaseService) {
    this.landDatabaseService = externalLandDatabaseService
    abind(this)
  }

  async create ({ plotInt, lienHolder, lienAmount, lienSender }) {
    const currentState = await this.landDatabaseService.getCurrentState(plotInt)

    if (isEmpty(currentState)) {
      throw new EventsDatabaseUnexpectedStateError('Empty sate found for plot')
    }

    if (`${lienSender}`.toLowerCase() !== `${currentState?.owner}`.toLowerCase()) {
      throw new EventsDatabaseUnexpectedStateError('Only owner is allowed to create a lien')
    }

    if (currentState?.lien === true) {
      throw new EventsDatabaseUnexpectedStateError('Land already has a lien')
    }

    const result = await this.landDatabaseService.putNewState({
      ...currentState,
      lien: true,
      lienAmount,
      lienHolder,
      timestamp: nowInMilliseconds(),
    })

    return result
  }
}
