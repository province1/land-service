import { isEmpty, isNil } from 'lodash'

import { EventsDatabaseUnexpectedStateError } from '../exceptions/events-database'
import { nowInMilliseconds } from '../utils/date'

export class OwnershipService {
  constructor (externalEventsDatabaseService) {
    this.externalEventsDatabaseService = externalEventsDatabaseService
  }

  async change ({ plotInt, newOwner }) {
    const currentState = await this.externalEventsDatabaseService.getCurrentState(plotInt)
    if (isEmpty(currentState)) {
      throw new EventsDatabaseUnexpectedStateError('Empty sate found for plot')
    }

    if (!isNil(currentState?.lien) && currentState.lien === true) {
      throw new EventsDatabaseUnexpectedStateError('Plot has a lien cannot change the ownership')
    }

    const result = await this.externalEventsDatabaseService.putNewState({
      ...currentState,
      owner: newOwner,
      plotInt,
      timestamp: nowInMilliseconds(),
    })

    return result
  }
}
