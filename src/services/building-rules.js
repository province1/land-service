import { isNil } from 'lodash'

// game rules for building
export class BuildingRules {
  static rule (buildingType, ruleName, ...args) {
    const ruleId = [buildingType, ruleName].filter(r => !isNil(r)).join('.')
    const rules = {
      'lumber-mill.materials': () => {
        return [['lumber', 20], ['copper', 10], ['timber', 4]]
      },
      'lumber-mill.maple-cost': () => {
        return 1000
      },
    }

    if (isNil(rules[ruleId]) || !(rules[ruleId] instanceof Function)) {
      throw new Error(`invalid or undefined rule ${ruleId}`)
    }

    return rules[ruleId](...args)
  }
}
