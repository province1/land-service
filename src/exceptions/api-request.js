import { BaseError } from './base-error'

export class APIRequestError extends BaseError { }

export class MaterialsAPIRequestError extends APIRequestError { }

export class FundsAPIRequestError extends APIRequestError { }

export class EquipmentAPIRequestError extends APIRequestError { }
