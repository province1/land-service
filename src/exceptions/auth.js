import StatusCodes from 'http-status-codes'

import { BaseError } from './base-error'

export class AuthenticationError extends BaseError {
  constructor (msg, originalError, code) {
    super(msg, originalError, code ?? StatusCodes.UNAUTHORIZED)
    this._msg = msg ?? `Authentication Error ${this?.msg ? ' - ' + this.msg : ''}`
  }
}

export class InvalidSignatureError extends BaseError {
  constructor (msg, originalError, code) {
    super(msg, originalError, code ?? StatusCodes.UNAUTHORIZED)
    this._msg = msg || 'Invalid signature'
  }
}

export class ExpiredSignatureError extends BaseError {
  constructor (msg, originalError, code) {
    super(msg, originalError, code ?? StatusCodes.GONE)
    this._msg = msg || 'Expired signature'
  }
}
