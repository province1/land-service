import StatusCodes from 'http-status-codes'
import { BaseError } from './base-error'

export class RequestValidationError extends BaseError {
  constructor (msg, originalError, code = StatusCodes.BAD_REQUEST) {
    super(msg, originalError, code)
  }
}
