import HttpStatus from 'http-status-codes'

import { HttpError } from './http-error'
import { err } from '../utils/error'
import { INVALID_URL } from './messages'

export default (error, req, res, next) => {
  if (res.headersSent) {
    return next(error)
  }

  res.status(error?.code ?? HttpStatus.INTERNAL_SERVER_ERROR)
  res.send(err(error))
}

export function NotFoundHandler () {
  throw new HttpError(INVALID_URL, undefined, HttpStatus.NOT_FOUND)
};
