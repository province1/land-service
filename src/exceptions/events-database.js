import StatusCodes from 'http-status-codes'

import { BaseError } from './base-error'

export class EventsDatabaseError extends BaseError {
  constructor (msg, originalError, code = StatusCodes.BAD_GATEWAY) {
    super(msg, originalError, code)
  }
}

export class EventsDatabaseUnexpectedStateError extends EventsDatabaseError {
  constructor (msg, originalError, code = StatusCodes.BAD_GATEWAY) {
    super(msg, originalError, code)
  }
}
