import HttpStatus from 'http-status-codes'

export class BaseError extends Error {
  constructor (msgOrError, originalError, code) {
    super(msgOrError)
    const msg = msgOrError instanceof Error ? msgOrError.message : msgOrError

    this._code = code ?? originalError?.code ?? HttpStatus.INTERNAL_SERVER_ERROR
    this._msg = msg ?? originalError?.msg ?? originalError?.message
    this._originalError = originalError
  }

  get msg () {
    return this._msg
  }

  get code () {
    return this._code
  }

  get originalError () {
    return this._originalError
  }
}
