export const INVALID_URL = 'Invalid URL'
export const BAD_QUERY_PARAMS = 'Missing or wrong query parameters'
