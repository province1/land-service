import StatusCodes from 'http-status-codes'

import { BaseError } from './base-error'

export class OperationalError extends BaseError {
  constructor (msg, originalError, code = StatusCodes.UNPROCESSABLE_ENTITY) {
    super(msg, originalError, code)
  }
}
