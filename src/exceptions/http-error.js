import HttpStatus from 'http-status-codes'
import { BaseError } from './base-error'

export class HttpError extends BaseError {
  constructor (msg, originalError, code = HttpStatus.INTERNAL_SERVER_ERROR) {
    super(msg, originalError, code)
  }
}
