import StatusCodes from 'http-status-codes'

import { BaseError } from './base-error'

export class EthersVerificationError extends BaseError {
  constructor (msg, originalError, code) {
    super(msg, originalError, code ?? StatusCodes.UNAUTHORIZED)
    this._msg = msg || 'Ethers verification failed'
  }
}

export class EthersContractOperationsError extends BaseError {
  constructor (msg, originalError, code) {
    super(msg, originalError, code ?? StatusCodes.GONE)
    this._msg = msg || 'Expired signature'
  }
}
