import { isNumericOrNumericString } from '../utils/type-checks/is-numeric-or-numeric-string'

export const MICRO_SERVICE_NAME = process.env.MICRO_SERVICE_NAME || 'land-service'
export const ENVIRONMENT = process.env.NODE_ENV || 'local'
export const IS_LOCAL_ENV = ENVIRONMENT === 'local'
export const IS_PRODUCTION = ENVIRONMENT === 'production' || ENVIRONMENT === 'prod'

export const ALLOWED_CORS_ORIGINS_REGEX = new RegExp(process.env.ALLOWED_CORS_ORIGINS_REGEX ?? /^.*$/)
export const PORT = process.env.PORT || 3000

export const EVENTS_DATABASE_SERVICE_API_KEY = process.env.EVENTS_DATABASE_SERVICE_API_KEY || 'very-secret-api-key'
export const EVENTS_DATABASE_SERVICE_API_KEY_HEADER = 'X-Api-Key'

export const EVENTS_DATABASE_SERVICE_URL = process.env.EVENTS_DATABASE_SERVICE_URL || 'http://localhost:3001'
export const EVENTS_DATABASE_SERVICE_USERNAME = process.env.EVENTS_DATABASE_SERVICE_USERNAME ?? 'anonymous'
export const EVENTS_DATABASE_SERVICE_PASSWORD = process.env.EVENTS_DATABASE_SERVICE_PASSWORD ?? ''

// TODO: this might be managed via some storage not via env vars
export const SALES_SERVICE_API_KEY = process.env.SALES_SERVICE_API_KEY
// default api key to use if there is no specific name for name it
export const GENERIC_SERVICE_API_KEY = process.env.GENERIC_SERVICE_API_KEY
export const ALLOWED_API_KEYS = [SALES_SERVICE_API_KEY, GENERIC_SERVICE_API_KEY]
export const BASIC_AUTH_USER = process.env.BASIC_AUTH_USER
export const BASIC_AUTH_PASSWORD = process.env.BASIC_AUTH_PASSWORD
export const MILLISECONDS_IN_A_DAY = 86400000 // 24 hr * 3600 sec/hr * 1000 millis/hr
export const _5_MIN_IN_MILLISECONDS = 5 * 60 * 1000
export const SIGNATURE_EXPIRATION_THRESHOLD_IN_MILLISECONDS =
  isNumericOrNumericString(process.SIGNATURE_EXPIRATION_THRESHOLD_IN_MILLISECONDS)
    ? Number(process.SIGNATURE_EXPIRATION_THRESHOLD_IN_MILLISECONDS)
    : _5_MIN_IN_MILLISECONDS

export const MATERIALS_SERVICE_URL = process.env.MATERIALS_SERVICE_URL || 'http://localhost:3002/api'
export const MATERIALS_SERVICE_BASIC_AUTH_USER = process.env.MATERIALS_SERVICE_BASIC_AUTH_USER
export const MATERIALS_SERVICE_BASIC_AUTH_PASSWORD = process.env.MATERIALS_SERVICE_BASIC_AUTH_PASSWORD

export const ETHERS_PROVIDER = process.env.ETHERS_PROVIDER ?? ''
export const MAPLE_ADDRESS = process.env.MAPLE_ADDRESS ?? ''
export const MAPLE_CONTRACT_FILE_PATH = process.env.MAPLE_CONTRACT_FILE_PATH ?? './maple-contract.json'
export const TREASURY_ADDRESS = process.env.TREASURY_ADDRESS ?? ''

export const FUNDS_SERVICE_URL = process.env.FUNDS_SERVICE_URL ?? ''
export const FUNDS_SERVICE_BASIC_AUTH = process.env.FUNDS_SERVICE_BASIC_AUTH ?? ''

export const EQUIPMENT_SERVICE_URL = process.env.EQUIPMENT_SERVICE_URL || 'http://localhost:3001/api'
export const DAYS_IN_YEAR_FLOAT = 365.25
