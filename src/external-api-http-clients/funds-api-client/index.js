import BaseHttpClient from '../base-http-client'
import {
  FUNDS_SERVICE_URL,
  FUNDS_SERVICE_BASIC_AUTH,
} from '../../config'
import { FundsAPIRequestError } from '../../exceptions/api-request'

export class FundsApiClient extends BaseHttpClient {
  constructor () {
    super({ baseURL: FUNDS_SERVICE_URL })

    this.addAxiosRequestInterceptor((axiosConfig) => {
      return ({
        ...axiosConfig,
        headers: {
          ...(axiosConfig.headers ?? {}),
          authorization: `Basic ${FUNDS_SERVICE_BASIC_AUTH}`,
        },
      })
    })
  }

  async transferAmount ({ sender, amount }) {
    try {
      const response = await this.post('/', { sender, amount: `${amount}` })

      return response
    } catch (error) {
      throw new FundsAPIRequestError(
        `Error during transfer: ${error.msg} ${error?.originalError?.data?.error_msg ?? ''}`,
        error,
      )
    }
  }
}
