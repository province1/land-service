import abind from 'abind'
import { isNil } from 'lodash'

import BaseHttpClient from '../base-http-client'
import {
  EQUIPMENT_SERVICE_URL,
} from '../../config'
import { EquipmentAPIRequestError } from '../../exceptions/api-request'

export class EquipmentApiClient extends BaseHttpClient {
  constructor () {
    super({ baseURL: EQUIPMENT_SERVICE_URL })
    abind(this)
  }

  async details ({ serial, detail }) {
    try {
      const encodedSerial = encodeURIComponent(serial)
      const encodedDetail = !isNil(detail) ? `/${encodeURIComponent(detail)}` : ''
      const response = await this.get(`/equipment-details/${encodedSerial}${encodedDetail}`)

      return response
    } catch (error) {
      throw new EquipmentAPIRequestError(
        `Unable to get the details: ${error.msg} ${error?.originalError?.data?.error_msg ?? ''}`,
        error,
      )
    }
  }
}
