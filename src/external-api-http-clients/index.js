import { FundsApiClient } from './funds-api-client'
import { MaterialsApiClient } from './materials-api-client'

export const materialsApiClient = new MaterialsApiClient()
export const fundsApiClient = new FundsApiClient()
