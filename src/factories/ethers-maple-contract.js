import { readFileSync } from 'fs'
import Moralis from 'moralis/node'

import { ETHERS_PROVIDER, MAPLE_ADDRESS, MAPLE_CONTRACT_FILE_PATH } from '../config'

const ethers = Moralis.web3Library

export class EthersMapleContractFactory {
  constructor () {
    try {
      this.mapleContract = JSON.parse(readFileSync(MAPLE_CONTRACT_FILE_PATH, 'utf-8').toString())
      this.provider = new ethers.providers.JsonRpcProvider(ETHERS_PROVIDER)
      this.ethersContract = new ethers.Contract(MAPLE_ADDRESS, this.mapleContract.abi, this.provider)
    } catch (error) {
      this.error = error
    }
  }

  produceSingleton () {
    if (this.error instanceof Error) { throw this.error }

    return this.ethersContract
  }

  produce () {
    return new ethers.Contract(MAPLE_ADDRESS, this.mapleContract.abi, this.provider)
  }
}
