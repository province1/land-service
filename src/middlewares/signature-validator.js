import Moralis from 'moralis/node'

import { IS_LOCAL_ENV, SIGNATURE_EXPIRATION_THRESHOLD_IN_MILLISECONDS } from '../config'
import { ExpiredSignatureError, InvalidSignatureError } from '../exceptions/auth'
import { EthersVerificationError } from '../exceptions/ethers'

import { nowInMilliseconds } from '../utils/date'

function isAnExpiredTimestamp (timestampInMilliseconds, threshold = SIGNATURE_EXPIRATION_THRESHOLD_IN_MILLISECONDS) {
  const now = nowInMilliseconds()

  return now - timestampInMilliseconds > threshold
}

export default function signatureValidator (req, res, next) {
  // accessing req.body for now is ok, since msg and signature is part of
  // the body and it has been validated by a validator before
  // it is expected a validator did it has valid data for validation
  // if that changes turn this function into HOC
  const { msg, signature } = req.body
  const {
    sender_address: senderAddress,
    timestamp: timestampInMilliseconds,
  } = JSON.parse(msg)
  try {
    const addressFromSignature = Moralis.web3Library.utils.verifyMessage(msg, signature)

    if (addressFromSignature.toLowerCase() !== senderAddress.toLowerCase()) {
      throw new InvalidSignatureError("Signature validation fail. Addresses doesn't match")
    }

    if (isAnExpiredTimestamp(Number(timestampInMilliseconds)) && !IS_LOCAL_ENV) {
      throw new ExpiredSignatureError()
    }

    next()
  } catch (error) {
    if (error instanceof InvalidSignatureError || error instanceof ExpiredSignatureError) {
      next(error)
    } else {
      next(new EthersVerificationError(error?.reason, error))
    }
  }
}
