import 'dotenv/config'
import { appBuilder } from './app-builder'

async function setupServer () {
  const { app } = await appBuilder()

  const httpServer = app.listen({ port: app.get('port') }, () => {
    console.log(
      `Server ready at http://localhost:${app.get('port')} in ${app.get('env')} mode`,
    )
  })
  httpServer.keepAliveTimeout = 121 * 1000
  httpServer.headersTimeout = 125 * 1000
}

setupServer()
