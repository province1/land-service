import swaggerJsdoc from 'swagger-jsdoc'
import packageJson from '../../../package.json'

const options = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'Land service',
      version: packageJson?.version || '0.0.0',
    },
  },
  apis: ['./src/routes/**/*.js'],
}

export const swaggerSpecification = swaggerJsdoc(options)
