import { isEmpty, isNil } from 'lodash'

export function isNumericOrNumericString (stringOrNumber) {
  if (typeof stringOrNumber === 'number') return !isNaN(stringOrNumber)
  return !isNil(stringOrNumber) && !isEmpty(stringOrNumber) && !isNaN(Number(stringOrNumber))
}
