import { Router } from 'express'

import injectOwnershipRoutes from './ownership'
import injectLienRoutes from './lien'
import injectLandRoutes from './land'
import injectBuildingsRoutes from './building'

const router = Router()

injectOwnershipRoutes({ router })
injectLienRoutes({ router })
injectLandRoutes({ router })
injectBuildingsRoutes({ router })

export default router
