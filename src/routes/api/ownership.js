import { ownershipController } from '../../controllers'
import changeOwnershipValidationSchema from '../../http-req-validation/validation-schemas/change-ownership'
import { buildValidationMiddleware } from '../../http-req-validation/validator'
import { basicAuthAuthenticator, buildAuthenticationMiddleware } from '../../middlewares/authentication'

const injectOwnershipRoutes = ({ router }) => {
  router.post(
    '/ownership',
    buildAuthenticationMiddleware(basicAuthAuthenticator),
    buildValidationMiddleware(changeOwnershipValidationSchema),
    ownershipController.changeOwnership,
  )
}

export default injectOwnershipRoutes
