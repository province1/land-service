import { lienController } from '../../controllers'
import addLienSchema from '../../http-req-validation/validation-schemas/add-lien'
import isSignedSchema from '../../http-req-validation/validation-schemas/is-signed'
import { buildValidationMiddleware } from '../../http-req-validation/validator'
import signatureValidator from '../../middlewares/signature-validator'

const injectLienRoutes = ({ router }) => {
  router.post(
    '/lien',
    buildValidationMiddleware(isSignedSchema),
    signatureValidator,
    buildValidationMiddleware(addLienSchema),
    lienController.create,
  )
}

export default injectLienRoutes
