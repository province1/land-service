import { landController } from '../../controllers'
import {
  clearLandSchema,
  stopClearLandSchema,
  startLandSaleSchema,
  finalizeLandSaleSchema,
  doLandChecksSchema,
} from '../../http-req-validation/validation-schemas/land-ops'
import isSignedSchema from '../../http-req-validation/validation-schemas/is-signed'
import { buildValidationMiddleware } from '../../http-req-validation/validator'
import signatureValidator from '../../middlewares/signature-validator'
import { basicAuthAuthenticator, buildAuthenticationMiddleware } from '../../middlewares/authentication'

const injectLandRoutes = ({ router }) => {
  /**
   * @openapi
   * /api/land/operation/clear:
   *   post:
   *     description: Start clearing process for a land
   *     tags:
   *       - land ground ops
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             $ref: '#/components/schemas/ClearLandBody'
   *     responses:
   *       200:
   *         content:
   *           $ref: '#/components/responses/CommonResponseObject'
   *       401:
   *         description: Invalid signature errors
   *         content:
   *           $ref: '#/components/responses/CommonResponseObject'
   *       400:
   *         description: Invalid request payload
   *         content:
   *           $ref: '#/components/responses/CommonResponseObject'
   *       502:
   *         description: Errors during inter service dialog such as unexpected states or errors from services we relay on
   *         content:
   *           $ref: '#/components/responses/CommonResponseObject'
   *       500:
   *         description: Internal errors
   *         content:
   *           $ref: '#/components/responses/CommonResponseObject'
   */
  router.post(
    '/land/operation/clear',
    buildValidationMiddleware(isSignedSchema),
    signatureValidator,
    buildValidationMiddleware(clearLandSchema),
    landController.clearLand,
  )

  /**
   * @openapi
   * /api/land/operation/clear/{plot_int}:
   *   parameters:
   *     - name: plot_int
   *       in: path
   *       required: true
   *       example: 1
   *       description: land's id
   *       schema:
   *         type: number
   *   delete:
   *     description: stop clearing a land
   *     tags:
   *       - land ground ops
   *     security:
   *       - basic_auth:
   *     responses:
   *       200:
   *         content:
   *           $ref: '#/components/responses/CommonResponseObject'
   *       401:
   *         description: authentication errors
   *         content:
   *           $ref: '#/components/responses/CommonResponseObject'
   *       400:
   *         description: Invalid request payload
   *         content:
   *           $ref: '#/components/responses/CommonResponseObject'
   *       502:
   *         description: Errors during inter service dialog such as unexpected states or errors from services we relay on
   *         content:
   *           $ref: '#/components/responses/CommonResponseObject'
   *       500:
   *         description: Internal errors
   *         content:
   *           $ref: '#/components/responses/CommonResponseObject'
   */
  router.delete(
    '/land/operation/clear/:plot_int/:owner',
    buildAuthenticationMiddleware(basicAuthAuthenticator),
    buildValidationMiddleware(stopClearLandSchema),
    landController.stopClearLand,
  )

  /**
   * @openapi
   * /api/land/operation/sale:
   *   post:
   *     description: start a sale
   *     tags:
   *       - land sales
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             $ref: '#/components/schemas/StartSaleBody'
   *     responses:
   *       200:
   *         content:
   *           $ref: '#/components/responses/CommonResponseObject'
   *       401:
   *         description: Invalid signature errors
   *         content:
   *           $ref: '#/components/responses/CommonResponseObject'
   *       400:
   *         description: Invalid request payload
   *         content:
   *           $ref: '#/components/responses/CommonResponseObject'
   *       502:
   *         description: Errors during inter service dialog such as unexpected states or errors from services we relay on
   *         content:
   *           $ref: '#/components/responses/CommonResponseObject'
   *       500:
   *         description: Internal errors
   *         content:
   *           $ref: '#/components/responses/CommonResponseObject'
   */
  router.post(
    '/land/operation/sale',
    buildValidationMiddleware(isSignedSchema),
    signatureValidator,
    buildValidationMiddleware(startLandSaleSchema),
    landController.startLandSale,
  )

  /**
   * @openapi
   * /api/land/operation/sale/{sale_id}/{plot_int}:
   *   parameters:
   *     - name: sale_id
   *       in: path
   *       required: true
   *       example: '8c126de5-60d1-4746-b491-f857934f56e9'
   *       description: valid UUID v4
   *     - name: plot_int
   *       in: path
   *       required: true
   *       example: 1
   *       description: land's id
   *       schema:
   *         type: number
   *   delete:
   *     description: finalize / cancel / abort a sale
   *     tags:
   *       - land sales
   *     security:
   *       - basic_auth:
   *     responses:
   *       200:
   *         content:
   *           $ref: '#/components/responses/CommonResponseObject'
   *       401:
   *         description: authentication errors
   *         content:
   *           $ref: '#/components/responses/CommonResponseObject'
   *       400:
   *         description: Invalid request payload
   *         content:
   *           $ref: '#/components/responses/CommonResponseObject'
   *       502:
   *         description: Errors during inter service dialog such as unexpected states or errors from services we relay on
   *         content:
   *           $ref: '#/components/responses/CommonResponseObject'
   *       500:
   *         description: Internal errors
   *         content:
   *           $ref: '#/components/responses/CommonResponseObject'
   */
  router.delete(
    '/land/operation/sale/:sale_id/:plot_int',
    buildAuthenticationMiddleware(basicAuthAuthenticator),
    buildValidationMiddleware(finalizeLandSaleSchema),
    landController.finalizeLandSale,
  )

  /**
   * @openapi
   * /api/land/operation/status-checks-trigger/{plot_int}:
   *   parameters:
   *     - name: plot_int
   *       in: path
   *       required: true
   *       example: 1
   *       description: land's id
   *       schema:
   *         type: number
   *   get:
   *     description: perform state transitions based on status and different timestamps
   *     tags:
   *       - land ground ops
   *     security:
   *       - basic_auth:
   *     responses:
   *       200:
   *         content:
   *           $ref: '#/components/responses/CommonResponseObject'
   *       401:
   *         description: authentication errors
   *         content:
   *           $ref: '#/components/responses/CommonResponseObject'
   *       400:
   *         description: Invalid request payload
   *         content:
   *           $ref: '#/components/responses/CommonResponseObject'
   *       502:
   *         description: Errors during inter service dialog such as unexpected states or errors from services we relay on
   *         content:
   *           $ref: '#/components/responses/CommonResponseObject'
   *       500:
   *         description: Internal errors
   *         content:
   *           $ref: '#/components/responses/CommonResponseObject'
   */
  router.get(
    '/land/operation/status-checks-trigger/:plot_int',
    buildAuthenticationMiddleware(basicAuthAuthenticator),
    buildValidationMiddleware(doLandChecksSchema),
    landController.execStatusChecks,
  )
}

export default injectLandRoutes

/*
State 0 - Maximum Tree Level (100% - 20 meters)
State 1 - Clear all treest (0% -0 Meters) 90 tons of timber - land_status: cleared
State 5 years (provincial time) - 50% tree level - 10 meters

Right now, we need to calculate the current "tree level" "on the fly" at the current time of the request - real time
Look at state management in the future

*/