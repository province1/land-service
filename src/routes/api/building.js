import { buildingController } from '../../controllers'
import { addBuildingSchema } from '../../http-req-validation/validation-schemas/building'
import isSignedSchema from '../../http-req-validation/validation-schemas/is-signed'
import { buildValidationMiddleware } from '../../http-req-validation/validator'
import signatureValidator from '../../middlewares/signature-validator'

const injectBuildingsRoutes = ({ router }) => {
  router.post(
    '/building/:building_type',
    buildValidationMiddleware(isSignedSchema),
    signatureValidator,
    buildValidationMiddleware(addBuildingSchema),
    buildingController.create,
  )
}

export default injectBuildingsRoutes
