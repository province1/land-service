import publicRoutes from './health-check'
import apiRoutes from './api'

/**
 *  @openapi
 * components:
 *   schemas:
 *     SignedClearLandPayload:
 *       type: string
 *       description: serialized json object containing sender_address and timestamp, plot_int attributes
 *       example: '{"plot_int":1,"sender_address":"0x14791697260E4c9A71f18484C9f997B308e59325","timestamp":1652710417786}'
 *     ClearLandBody:
 *       type: object
 *       required:
 *         - msg
 *         - signature
 *       properties:
 *         msg:
 *           $ref: '#/components/schemas/SignedClearLandPayload'
 *         signature:
 *           type: string
 *     SignedStartSalePayload:
 *       type: string
 *       description: serialized json object containing plot_int, sender_address attributes
 *       example: '{"plot_int":1,"sender_address":"0x14791697260E4c9A71f18484C9f997B308e59325"}'
 *     StartSaleBody:
 *       type: object
 *       required:
 *         - msg
 *         - signature
 *       properties:
 *         msg:
 *           $ref: '#/components/schemas/SignedStartSalePayload'
 *         signature:
 *           type: string
 *   responses:
 *     CommonResponseObject:
 *       content:
 *         schema:
 *           type: object
 *           required:
 *             - error
 *             - error_msg
 *             - error_code
 *           properties:
 *             error:
 *               type: boolean
 *               description: true for an failed operation false otherwise
 *             error_msg:
 *               type: string
 *               description: error description
 *             error_code:
 *               type: number
 *               description: error code number
 *             results:
 *               oneOf:
 *                 - type: object
 *                 - type: array
 *                   items:
 *                     - type: object
 *   securitySchemes:
 *     basic_auth:
 *       description: Basic http user pass authentication
 *       type: http
 *       scheme: basic
 */

export default function ({ app }) {
  /**
   * @openapi
   *
   * /health-check:
   *   get:
   *     description: Health check endpoint
   *     produces:
   *       - application/json
   */
  app.use('/health-check', publicRoutes)

  /**
   * @openapi
   *
   * servers:
   *   - url: http://localhost:3000/api
   *     description: Development server
   *   - url: https://fqdn.com/api
   *     description: Development server
   */
  app.use('/api', apiRoutes)

  return app
}
