import { Router } from 'express'

import { publicController } from '../controllers'

const router = Router()

router.get('/', publicController.healthCheck)

export default router
