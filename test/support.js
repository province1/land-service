import chai from 'chai'
import sinonChai from 'sinon-chai'
import charAsPromised from 'chai-as-promised'
import sinon from 'sinon'

chai.use(sinonChai)
chai.use(charAsPromised)

global.sinon = sinon
global.expect = chai.expect
