import * as adapter from 'data-service-adapter'
import { EventsDatabaseService } from '../../src/external-service-integrations/events-database'

describe('test events data base service api call', () => {
  afterEach(() => {
    sinon.restore()
  })

  it('should call the underlying service', async () => {
    const stubOp = sinon.stub()
    const stub = sinon.stub(adapter, 'database_service')
    stub.returns({
      data_op: stubOp
    })

    const eventsDatabaseService = new EventsDatabaseService(adapter.database_service)
    await eventsDatabaseService.getCurrentState()
    expect(stubOp).to.be.calledWith({ collection: 'land_data' }, 'current_state')
  })
})
