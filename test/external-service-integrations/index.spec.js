import { database_service as databaseService } from 'data-service-adapter'
describe('test', () => {
  it('databaseService to be a function', () => {
    expect(databaseService).to.be.instanceof(Function)
  })
})
